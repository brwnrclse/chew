#!/usr/bin/env node
const
  faviconsSrc = `assets/favicon/favicon.png`,
  faviconsCfg = {
    appName: `chew`,
    appDescription: `Homepage of mcDJ chew`,
    developerName: `brwnrclse (Barry Harris)`,
    developerURL: `https://brwnrclse.xyz`,
    path: `/`,
    url: `https://chew.brwnrclse.xyz`,
    display: `standalone`,
    orientation: `portrait`,
    version: `1.0`,
    logging: false,
    online: false,
    icons: {
      android: false,
      appleIcon: true,
      appleStartup: false,
      favicons: true,
      firefox: false,
      twitter: false,
      windows: false,
    }
  },
  faviconsCb = (err, res) => {
    if (err) {
      throw err;
    }

    res.images.map((image) => {
      require(`fs`).writeFileSync(`public/assets/favicon/${image.name}`,
        image.contents);
    });
  };

require(`favicons`)(faviconsSrc, faviconsCfg, faviconsCb);
