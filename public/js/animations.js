const
  catalog = document.querySelector(`#catalog`),
  catalogClose = document.querySelector(`#catalog-close`),
  catalogText = document.querySelector(`.chew-catalog-content`),
  catalogWindow = document.querySelector(`.chew-catalog-window-bar`),

  player = document.querySelector(`#player`),
  playerClose = document.querySelector(`#player-close`),
  playerText = document.querySelector(`.chew-player-content`),
  playerWindow = document.querySelector(`.chew-player-window-bar`),

  notFoundIndex = -1;

/**
 * Shows the catalog content.
 *
 * @returns {undefined}
 */
function showCatalogContent () {
  catalogText.id = `catalog`;
  catalogWindow.id = `window-bar`;
}

/**
 * Hides the catalog content.
 *
 * @returns {undefined}
 */
function hideCatalogContent () {
  catalogText.id = `hide`;
  catalogWindow.id = `hide`;
}

/**
 * Shows the player content.
 *
 * @returns {undefined}
 */
function showPlayerContent () {
  playerText.id = `player`;
  playerWindow.id = `window-bar`;
}

/**
 * Hides the player content.
 *
 * @returns {undefined}
 */
function hidePlayerContent () {
  playerText.id = `hide`;
  playerWindow.id = `hide`;
}

/**
 * Handles showing and hiding text components based on the current animation for
 *       the catalog component.
 *
 * @param      {Event}  evt       The event we'll use to decide an action
 *
 * @returns {undefined}
 */
function catalogAnimationHandler (evt) {
  if (evt.type.indexOf(`End`) !== notFoundIndex &&
      evt.animationName === `slide-out`) {
    showCatalogContent();
  } else if (evt.animationName === `slide-in`) {
    hideCatalogContent();
  }
}

/**
 * Handles showing and hiding text components based on the current animation for
 *       the player component.
 *
 * @param      {Event}  evt   The event we'll use to decide an action
 *
 * @returns {undefined}
 */
function playerAnimationHandler (evt) {
  if (evt.type.indexOf(`End`) !== notFoundIndex &&
      evt.animationName === `slide-out`) {
    showPlayerContent();
  } else if (evt.animationName === `slide-in`) {
    hidePlayerContent();
  }
}

/**
 * Handles click operations for the catalog component
 *
 *
 * @param      {Event}  evt       The event we'll use to decide an action
 *
 * @returns {undefined}
 */
function catalogClickHandler (evt) {
  if (evt.target.id === `catalog-close`) {
    catalog.className = `chew-catalog slide-in`;
  } else if (catalog.className.indexOf(`slide-out`) === notFoundIndex) {
    catalog.className = `chew-catalog slide-out`;
  }
}

/**
 * Handles click actions for the player component
 *
 *
 * @param      {Event}  evt       The event we'll use to decide an action
 *
 * @returns {undefined}
 */
function playerClickHandler (evt) {
  if (evt.target.id === `player-close`) {
    player.className = `chew-player slide-in`;
  } else if (player.className.indexOf(`slide-out`) === notFoundIndex) {
    player.className = `chew-player slide-out`;
  }
}

catalog.addEventListener(`webkitAnimationEnd`, catalogAnimationHandler, false);
catalog.addEventListener(`animationEnd`, catalogAnimationHandler, false);

catalog.addEventListener(`webkitAnimationStart`, catalogAnimationHandler,
                          false);
catalog.addEventListener(`animationStart`, catalogAnimationHandler, false);

catalog.addEventListener(`click`, catalogClickHandler, false);

catalogClose.addEventListener(`click`, catalogClickHandler, false);

player.addEventListener(`webkitAnimationEnd`, playerAnimationHandler, false);
player.addEventListener(`animationEnd`, playerAnimationHandler, false);

player.addEventListener(`webkitAnimationStart`, playerAnimationHandler, false);
player.addEventListener(`animationStart`, playerAnimationHandler, false);

player.addEventListener(`click`, playerClickHandler, false);

playerClose.addEventListener(`click`, playerClickHandler, false);
